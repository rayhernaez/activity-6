public abstract class VegetarianSandwich implements ISandwich {
    private String filling;

    //method 
    public void addFilling(String topping) {
        String[] unwanted_toppings = {"chicken", "beef", "fish", "meat", "pork"};
        for (int i = 0; i < unwanted_toppings.length; i++) {
            if (topping == unwanted_toppings[i]) {
                throw new UnsupportedOperationException("Unwanted Topping");
            }
        }
        this.filling += ", " + topping;
    }

    public boolean isVegetarian() {
        return true;
    }

    public boolean isVegan() {
        if (this.filling.contains("cheese") || this.filling.contains("egg")) {
            return false;
        }
        else {
            return true;
        }
    }

    //abstract method
    public abstract String getProtein();

}
