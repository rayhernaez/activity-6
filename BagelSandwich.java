public class BagelSandwich implements ISandwich {
    protected String filling;

    //constructor
    public BagelSandwich() {
        this.filling = "";
    }

    //methods
    public void addFilling(String topping) {
        this.filling += ", " + topping;
    }

    public boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }

    //getter
    public String getFilling() {
        return this.filling;
    }
}
